package jm.edu.utech.welcome.contracts;

public interface IWelcomeService {
    String getWelcomeMessage(String name);
}
