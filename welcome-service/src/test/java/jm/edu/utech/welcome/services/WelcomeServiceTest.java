package jm.edu.utech.welcome.services;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class WelcomeServiceTest {
	
	@Test
	public void shouldReceiveWelcomeMessage() {
		WelcomeService service = new WelcomeService();
		assertEquals("Welcome Tom",service.getWelcomeMessage("Tom"));
	}

}
